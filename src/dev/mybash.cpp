/**
 * Operating System Course Design Experiment 10.
 * Cpp implementation of Simple Shell Command Line Interpreter.
 *
 * Author: Roc
 *
 * cd <目录>更改当前的工作目录到另一个<目录>。
 * environ 列出所有环境变量字符串的设置
 * echo <内容 > 显示 echo 后的内容且换行
 * help 简短概要的输出你的 shell 的使用方法和基本功能。
 * jobs 输出 shell 当前的一系列子进程，必须提供子进程的命名和 PID 号。
 * quit,exit,bye 退出 shell。
 */

#include <sys/wait.h>
#include <unistd.h>

#include <iostream>
#include <string>

using namespace std;

const string help =
    "Roc's bash, version 0.1.0\n"
    "Type 'help' to see commands list\n\n"
    " Command                Discribe\n"
    " cd                 change the current working directory to another\n"
    " environ            list all environment variable string settings\n"
    " echo               display the content after echo and wrap\n"
    " help               a brief summary of the usage of this shell\n"
    " jobs               output the current subprocesses of the shell\n"
    " quit,exit,bye      exit shell\n";
const string promote =
    "Roc's bash, version 0.1.0\n"
    "Type 'help' to see commands list\n\n";

void fun_cd(string command)
{
    char cwd[100];
    string arg = command.substr(2);
    if (arg[0] != ' ') {
        return;
    }
    arg.erase(0, arg.find_first_not_of(" "));
    arg.erase(arg.find_last_not_of(" ") + 1);

    const char* c_dic = arg.c_str();
    if (arg.empty()) {
        cout << "Please enter the directory\n";
    } else {
        if (chdir(c_dic) != 0) {
            cout << "Directory opening failed: " << arg << endl;
        } else {
            getcwd(cwd, 100);
            cout << "Current working directory: " << cwd << endl;
        }
    }
}

void fun_environ(string command, pid_t& pid)
{
    if ((pid = fork()) < 0) {
        printf("   fork error\n");
    } else if (pid == 0) {
        if (execl("/bin/env", "env", NULL) < 0) {
            printf("   execl error\n");
        }
    }
    waitpid(pid, 0, 0);
}

void fun_echo(string command)
{
    string arg = command.substr(4);
    if (arg[0] != ' ') {
        return;
    }
    arg.erase(0, arg.find_first_not_of(" "));
    arg.erase(arg.find_last_not_of(" ") + 1);
    cout << arg << endl;
}

void fun_help()
{
    cout << help;
}

void fun_jobs()
{
    system("ps");
}

void fun_exit(bool& exit)
{
    exit = true;
    cout << "bye\n";
}

void fun_undefine(string command)
{
    command.erase(command.find_first_of(" "));
    cout << "command not found: " << command << endl;
}

int main()
{
    bool exit = false;
    string command;
    char cwd[100];
    pid_t pid;

    cout << promote;
    while (!exit) {
        getcwd(cwd, 100);
        cout << cwd << " $ ";
        getline(cin, command);
        command.erase(0, command.find_first_not_of(" "));
        command.erase(command.find_last_not_of(" ") + 1);

        switch (command[0]) {
            case 'b':
                if (command == "bye") {
                    fun_exit(exit);
                }
                break;

            case 'c':
                if (command[1] == 'd') {
                    fun_cd(command);
                }

                break;

            case 'e':
                if (command == "environ") {
                    fun_environ(command, pid);
                } else if (command[1] == 'c' && command[2] == 'h' &&
                           command[3] == 'o') {
                    fun_echo(command);
                } else if (command == "exit") {
                    fun_exit(exit);
                }

                break;

            case 'h':
                if (command == "help") {
                    fun_help();
                }
                break;

            case 'j':
                if (command == "jobs") {
                    fun_jobs();
                }
                break;

            case 'q':
                if (command == "quit") {
                    fun_exit(exit);
                }
                break;

            default:
                fun_undefine(command);
                break;
        }
    }

    return 0;
}
