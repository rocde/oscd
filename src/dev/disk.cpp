/**
 * Operating System Course Design Experiment 7.
 * Cpp implementation of Disk Scheduling Algorithm.
 *
 * Author: Roc
 */

#include <limits.h>

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

void fun_print(vector<int> queue, vector<int> lens)
{
    double res = 0;
    cout << "   Next track   Length\n";
    for (int i = 0; i < queue.size(); i++) {
        res = res + lens[i];
        cout << "\t" << queue[i] << "\t" << lens[i] << endl;
    }
    res = res / queue.size();
    cout << "Average seek length: " << res << endl;
}

void fun_FIFO(vector<int> tracks, int cur)
{
    vector<int> queue, lens;
    int length = 0;
    vector<int>::iterator it;
    for (it = tracks.begin(); it != tracks.end(); it++) {
        length = abs(cur - *it);
        cur = *it;
        lens.push_back(length);
        queue.push_back(cur);
    }
    cout << "------------------------------\n"
            "FIFO(Start from track 100)\n";
    fun_print(queue, lens);
}

void fun_SSTF(vector<int> tracks, int cur)
{
    vector<int> queue, lens;
    int flag;
    int gap;
    int length = 0;
    int unfinish = tracks.size();
    while (unfinish--) {
        flag = 0;
        gap = INT_MAX;
        for (int i = 0; i < tracks.size(); i++) {
            if (tracks[i] < 0) {
                continue;
            }
            if (abs(cur - tracks[i]) < gap) {
                gap = abs(cur - tracks[i]);
                flag = i;
            }
        }
        length = abs(cur - tracks[flag]);
        cur = tracks[flag];
        tracks[flag] = -1;
        queue.push_back(cur);
        lens.push_back(length);
    }
    cout << "------------------------------\n"
            "SSTF(Start from track 100)\n";
    fun_print(queue, lens);
}

// TODO increase and decrease
void fun_SCAN(vector<int> tracks, int cur)
{
    int length = 0;

    vector<int> small, large, lens;
    vector<int>::iterator it;
    for (it = tracks.begin(); it != tracks.end(); it++) {
        if (*it < cur) {
            small.push_back(*it);
        } else {
            large.push_back(*it);
        }
    }

    sort(small.rbegin(), small.rend());
    sort(large.begin(), large.end());

    vector<int> res = large;
    res.insert(res.end(), small.begin(), small.end());

    for (int i = 0; i < res.size(); i++) {
        if (i == 0) {
            lens.push_back(abs(100 - res[i]));
        } else {
            lens.push_back(abs(res[i] - res[i - 1]));
        }
    }

    cout << "------------------------------\n"
            "SCAN(Start from track 100)\n";
    fun_print(res, lens);
}

void fun_CSCAN(vector<int> tracks, int cur)
{
    int length = 0;
    sort(tracks.begin(), tracks.end());

    vector<int> small, large, lens;
    vector<int>::iterator it;
    for (it = tracks.begin(); it != tracks.end(); it++) {
        if (*it < cur) {
            small.push_back(*it);
        } else {
            large.push_back(*it);
        }
    }

    sort(small.begin(), small.end());
    sort(large.begin(), large.end());

    vector<int> res = large;
    res.insert(res.end(), small.begin(), small.end());

    for (int i = 0; i < res.size(); i++) {
        if (i == 0) {
            lens.push_back(abs(100 - res[i]));
        } else {
            lens.push_back(abs(res[i] - res[i - 1]));
        }
    }

    cout << "------------------------------\n"
            "C-SCAN(Start from track 100)\n";
    fun_print(res, lens);
}

int main()
{
    vector<int> tracks = {55, 58, 39, 18, 90, 160, 150, 38, 184};
    int initial = 100;

    fun_FIFO(tracks, initial);
    fun_SSTF(tracks, initial);
    fun_SCAN(tracks, initial);
    fun_CSCAN(tracks, initial);
    return 0;
}
