/**
 * Operating System Course Design Experiment 6.
 * Cpp implementation of Banker's Algorithm.
 *
 * Author: Roc
 */

#include <iostream>
#include <set>
#include <vector>

using namespace std;

typedef struct Status {
    int m;                       // number of resourses
    int n;                       // number of processes
    vector<int> resource;        // total amount of system resources
    vector<int> available;       // total amount of unallocated resources
    vector<vector<int>> claim;   // request of each process
    vector<vector<int>> alloc;   // resources currently allocated to the process
    vector<vector<int>> demand;  // Demand matrix demand=claim-alloc
} status;

void fInput(status& s)
{
    cout << "Please enter the number of resource types: ";
    cin >> s.m;

    cout << "PLease enter the number of processes: ";
    cin >> s.n;

    // s.pstauts = vector<bool>(s.n, false);
    s.resource = vector<int>(s.m, 0);
    s.available = vector<int>(s.m, 0);
    s.claim = vector<vector<int>>(s.n, vector<int>(s.m, 0));
    s.alloc = vector<vector<int>>(s.n, vector<int>(s.m, 0));
    s.demand = vector<vector<int>>(s.n, vector<int>(s.m, 0));

    cout << "Please enter the available list(available): ";
    for (int i = 0; i < s.m; i++) {
        cin >> s.available[i];
    }

    cout << "Please enter the claim matrix(claim): \n";
    for (int i = 0; i < s.n; i++) {
        for (int j = 0; j < s.m; j++) {
            cin >> s.claim[i][j];
        }
    }

    cout << "Please enter the allocate matrix(alloc): \n";
    for (int i = 0; i < s.n; i++) {
        for (int j = 0; j < s.m; j++) {
            cin >> s.alloc[i][j];
        }
    }

    // getDemand
    for (int i = 0; i < s.n; i++) {
        for (int j = 0; j < s.m; j++) {
            s.demand[i][j] = s.claim[i][j] - s.alloc[i][j];
        }
    }
}

void fOutput(status s)
{
#ifdef __linux__
    system("clear");
#elif _WIN32
    system("cls");
#endif
    cout << "---------------------------------------------------------------\n";
    cout << "number of resource types: " << s.m << endl;
    cout << "number of processes: " << s.n << endl;
    cout << "available vector: " << endl;
    for (int i = 0; i < s.m; i++) {
        cout << "\t" << s.available[i];
    }
    cout << endl << "Claim matrix: " << endl;
    for (int i = 0; i < s.n; i++) {
        for (int j = 0; j < s.m; j++) {
            cout << "\t" << s.claim[i][j];
        }
        cout << endl;
    }
    cout << "Allocation matrix: " << endl;
    for (int i = 0; i < s.n; i++) {
        for (int j = 0; j < s.m; j++) {
            cout << "\t" << s.alloc[i][j];
        }
        cout << endl;
    }
    cout << "Needs matrix(C-A): " << endl;
    for (int i = 0; i < s.n; i++) {
        for (int j = 0; j < s.m; j++) {
            cout << "\t" << s.demand[i][j];
        }
        cout << endl;
    }
    cout << "---------------------------------------------------------------\n";
}

bool compare(const vector<int>& a, const vector<int>& b, int len)
{
    for (int i = 0; i < len; i++) {
        if (a[i] > b[i]) {
            return false;
        }
    }
    return true;
}

bool safecheck(status& s, vector<int>& safe)
{
    vector<int> work = s.available;
    set<int> unfinish;
    for (int i = 0; i < s.n; i++) {
        unfinish.insert(i);
    }

    for (int i = 0; i < s.n; i++) {
        for (int j = 0; j < s.n; j++) {
            if (unfinish.find(j) == unfinish.end()) {
                continue;
            } else {
                if (compare(s.demand.at(j), work, s.m)) {
                    for (int k = 0; k < s.m; k++) {
                        work[k] = work[k] + s.alloc[j][k];
                    }
                    // cout << "p" << j << endl;
                    safe.push_back(j);
                    unfinish.erase(j);
                }
            }
            if (unfinish.empty()) {
                return true;
            }
        }
        if (unfinish.empty()) {
            return true;
        }
    }
    return false;
}

int main()
{
    vector<int> queue;
    status s;
    bool found = false;

    fInput(s);
    fOutput(s);
    if (safecheck(s, queue)) {
        cout << "This status is safe.\nOne of the safe queues is:\n";
        for (int i = 0; i < queue.size(); i++) {
            cout << "p" << queue[i];
            if (i != queue.size() - 1) {
                cout << " -> ";
            }
        }
        cout << endl;
    } else {
        cout << "This status is unsafe." << endl;
    }
    return 0;
}
