/**
 * Operating System Course Design Experiment 9.
 * Cpp implementation of Concurrent programming for Dining Philosopher Problem.
 *
 * Author: Roc
 */

#include <stdio.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <unistd.h>

#include <cassert>
#include <iostream>

using namespace std;

#define N 5
int room;
int forks;

union semun {
    int val;               /* Value for SETVAL */
    struct semid_ds *buf;  /* Buffer for IPC_STAT, IPC_SET */
    unsigned short *array; /* Array for GETALL, SETALL */
    struct seminfo *__buf; /* Buffer for IPC_INFO
                              (Linux-specific) */
};

// struct sembuf {
//     unsigned short int sem_num; /* semaphore number */
//     short int sem_op;           /* semaphore operation */
//     short int sem_flg;          /* operation flag */
// };

void think(int i)
{
    cout << "Philosopher " << i << " is thinking\n";
}

void eat(int i)
{
    cout << "Philosopher " << i << " is eating\n";
}

// Creat and initialize semaphore
bool sem_init()
{
    forks = semget(IPC_PRIVATE, 5, IPC_CREAT | 0777);
    room = semget(IPC_PRIVATE, 1, IPC_CREAT | 0777);

    assert(forks != -1);
    assert(room != -1);

    union semun sem_union;
    sem_union.val = 1;
    for (int i = 0; i < N; i++) {
        if (semctl(forks, i, SETVAL, sem_union) == -1) {
            return false;
        }
    }

    sem_union.val = 4;
    if (semctl(room, 0, SETVAL, sem_union) == -1) {
        return false;
    }

    cout << "Semaphore creat success\n";
    return true;
}

// P operation for forks
bool sem_forks_p(int philosopher, int fork)
{
    struct sembuf sem_buf = {(unsigned short int)fork, -1, SEM_UNDO};

    if (semop(forks, &sem_buf, 1) == -1) {
        return false;
    }

    cout << "Philosopher " << philosopher << " pick up fork " << fork << endl;
    return true;
}

// V operation for forks
bool sem_forks_v(int philosopher, int fork)
{
    struct sembuf sem_buf = {(unsigned short int)fork, 1, SEM_UNDO};

    if (semop(forks, &sem_buf, 1) == -1) {
        return false;
    }

    cout << "Philosopher " << philosopher << " put down fork " << fork << endl;
    return true;
}

// P operation for room
bool sem_room_p(int philosopher)
{
    struct sembuf sem_buf = {0, -1, SEM_UNDO};

    if (semop(room, &sem_buf, 1) == -1) {
        return false;
    }

    cout << "Philosopher " << philosopher << " enter" << endl;
    return true;
}

// V operation for room
bool sem_room_v(int philosopher)
{
    struct sembuf sem_buf = {0, 1, SEM_UNDO};

    if (semop(room, &sem_buf, 1) == -1) {
        return false;
    }

    cout << "Philosopher " << philosopher << " go out" << endl;
    return true;
}

void philosopher(int i)
{
    srand(time(0));
    while (true) {
        think(i);
        sleep(rand() % 3 + 1);
        sem_room_p(i);
        sem_forks_p(i, i);
        sem_forks_p(i, (i + 1) % 5);
        eat(i);
        sleep(rand() % 4 + 1);
        sem_forks_v(i, (i + 1) % 5);
        sem_forks_v(i, i);
        sem_room_v(i);
    }
}

int main()
{
    pid_t pid;
    if (!sem_init()) {
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < N; i++) {
        pid = fork();
        if (pid < 0) {
            cout << "Fork failed\n";
            return 1;
        } else if (pid == 0) {
            philosopher(i);
        }
    }
    return 0;
}
