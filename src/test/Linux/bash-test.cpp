#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
char command(char *s)
{
    if (!strncasecmp(s, "exit", 4))
        return 'q';
    else if (!strncasecmp(s, "quit", 4))
        return 'q';
    else if (!strncasecmp(s, "bye", 4))
        return 'q';
    else if (!strncasecmp(s, "cd", 2))
        return 1;
    else if (!strncasecmp(s, "ls", 2))
        return 2;
    else if (!strncasecmp(s, "rm", 2))
        return 3;
    else if (!strncasecmp(s, "mkdir", 5))
        return 4;
    else if (!strncasecmp(s, "echo", 4))
        return 5;
    else if (!strncasecmp(s, "help", 4))
        return 6;
    else if (!strncasecmp(s, "env", 3))
        return 7;
    else if (!strncasecmp(s, "jobs", 4))
        return 8;
    else if (!strncasecmp(s, "clean", 5))
        return 9;
    else
        return 0;
}
void help()
{
    printf(
        "\t*****************帮助********************\n"
        "\t*   命令                     功能\n"
        "\t*   cd            更改当前的工作目录到另一个<目录>\n"
        "\t*   environ       列出所有环境变量字符串的设置\n"
        "\t*   echo          显示echo后面的内容并换行\n"
        "\t*   help          显示帮助信息\n"
        "\t*   jobs          "
        "输出shell当前的一系列子进程，包括子进程的命名和PID号\n"
        "\t*   ls            显示当前目录的所有文件\n"
        "\t*   rm            删除当前目录下的文件或目录\n"
        "\t*   mkdir         在当前目录下创建新目录\n"
        "\t*   clean         清屏\n"
        "\t*   quit|exit|bye 退出shell\n"
        "\t*****************************************\n");
}
int main()
{
    char n[100], n1[100];
    char b;
    pid_t pid;
    help();
    while (1) {
        memset(n, 0, 100);
        printf("%s_@_$：", getcwd(n1, 100));  // print the information;
        fgets(n, 100, stdin);                 // input cmdline;
        n[strlen(n) - 1] = '\0';              // delete the '\n';
        b = command(n);                       // get the return values;
        if (b == 'q')
            break;  // exit the shell;
        switch (b) {
            // change the directory
            case 1:
                if (chdir(n + 3) != 0)
                    printf(
                        "   打开工作目录(%s)失败!\n",
                        n + 3);  //+3  =
                                 //'c','d','\0',后面的数字为输入命令的字符个数加+1（‘\0’占一个字符）;
                printf("   当前工作目录：'%s'\n", getcwd(n1, 100));
                break;
            // list
            case 2:
                if ((pid = fork()) < 0) {
                    printf("   fork error\n");
                    exit(EXIT_FAILURE);
                } else if (pid == 0) {
                    if (execl("/bin/ls", "ls", NULL) < 0)
                        ;
                    printf("   execl error\n");
                    exit(EXIT_FAILURE);
                }
                waitpid(pid, 0, 0);
                break;
            // remove a directory
            case 3:
                remove(n + 3);
                printf("   文件已删除!\n");
                break;
            // make a directory
            case 4:
                mkdir(n + 6, S_IRWXU);
                printf("   文件创建成功!\n");
                break;
            // print something
            case 5:
                printf("   %s\n", n + 5);
                break;
            // help
            case 6:
                help();
                break;
            // environment
            case 7:
                if ((pid = fork()) < 0) {
                    printf("   fork error\n");
                    exit(EXIT_FAILURE);
                } else if (pid == 0) {
                    if (execl("/bin/env", "env", NULL) < 0)
                        printf("   execl error\n");
                    exit(EXIT_FAILURE);
                }
                waitpid(pid, 0, 0);
                break;
            // ps the running process ;
            case 8:
                system("ps");  // systemcall_ps
                break;
            case 9:
                system("clear");
                break;
            // cmd can't find;
            case 0:
                printf("PID: %ld.没有此命令，请重新输入正确的命令。\n",
                       (long)getpid());
                help();
                break;
        }
    }
    return 0;
}
