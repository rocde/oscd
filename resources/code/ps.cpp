/*  附录的源程序 */
/*  Remove newlines */
#include <stdio.h>
main()
{
    int c, n = 0, max = 1;
    while ((c = getchar()) != EOF) {
        if (c == '\n')
            n++;
        else
            n = 0;
        if (n <= max)
            putchar(c);
    }
}